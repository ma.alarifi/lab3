package LinearAlgebra;

public class Problem {
	public static void main(String[] args) {
		Vector3d a = new Vector3d(1,2,3);
		Vector3d b = new Vector3d(5,6,7);
		
		System.out.println(a.getX());
		System.out.println(a.add(b));
		System.out.println(a.dotProduct(b));
		System.out.println(a.magnitude());
		System.out.println(b.magnitude());
	}
}
