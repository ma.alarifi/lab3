package LinearAlgebra;
import java.lang.Math;

public class Vector3d {
	private double x;
	private double y;
	private double z;
	
	public Vector3d(double newX, double newY, double newZ){
		x = newX;
		y = newY;
		z = newZ;
	}
	
	public double getX(){
		return x;
	}
	
	public double getY(){
		return y;
	}
	
	public double getZ(){
		return z;
	}
	
	public double magnitude(){
		return Math.sqrt(Math.pow(x, 2)+Math.pow(y, 2)+Math.pow(z, 2));
	}
	
	public double dotProduct(Vector3d vectorB){
		return x*vectorB.getX()+y*vectorB.getY()+z*vectorB.getZ();
	}
	
	public Vector3d add(Vector3d vectorB){
		Vector3d sum = new Vector3d(x+vectorB.getX(), y+vectorB.getY(), z+vectorB.getZ());
		return sum;
	}
	
	public String toString(){
		return x + " "+ y + " "+ z;
	}
}

