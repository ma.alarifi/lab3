package LinearAlgebra;

import junit.framework.TestCase;

public class Vector3dTests extends TestCase {
	
	@Test
	public void testGetMethods(){
		Vector3d a = new Vector3d(1,2,3);
		assertEquals(1.0, a.getX());
		assertEquals(2.0, a.getY());
		assertEquals(3.0, a.getZ());
	}
	
	@Test
	public void testMagnitude(){
		Vector3d a = new Vector3d(2,3,6);
		assertEquals(7.0,a.magnitude());
	}
	
	@Test
	public void testDotProduct(){
		Vector3d a = new Vector3d(1,1,0);
		Vector3d b = new Vector3d(1,-1,-1);
		assertEquals(0.0, a.dotProduct(b));
	}
	
	@Test
	public void testAdd(){
		Vector3d a = new Vector3d(1,2,3);
		Vector3d b = new Vector3d(4,5,6);
		assertEquals(5.0 ,a.add(b).getX());
		assertEquals(7.0 ,a.add(b).getY());
		assertEquals(9.0 ,a.add(b).getZ());
	}
}
